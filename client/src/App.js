import React, { Fragment } from "react";
import {
  BrowserRouter as Router,
  Routes,
  Route,
  Link
} from "react-router-dom";

export default function App() {
  return (
    <Router basename={process.env.PUBLIC_URL}>
      <div>
        <nav>
          <ul>
            <li>
              <Link to="/">Learn React</Link>
            </li>
            <li>
              <Link to="/about">About</Link>
            </li>
            <li>
              <Link to="/users">Users</Link>
            </li>
          </ul>
        </nav>

        <Routes>
          <Route path="/about" element={ <About /> } />
          <Route path="/users" element={ <Users /> } />
          <Route path="/" element={ <Home /> } />
        </Routes>
      </div>
    </Router>
  );
}

function Home() {
  return (<Fragment>
    <h2>Home</h2>
    <p>Greetings.</p>
  </Fragment>);
}

function About() {
  return <h2>About</h2>;
}

function Users() {
  return <h2>Users</h2>;
}
